"""@file        encoder.py
   @brief       Interacts with encoder hardware
   @details     Creates a set of useful functions which the user can 
                eventually call
             
   @author      Aleya Dolorfino
   @author      Tanner Hillman
   @date        October 8, 2021
"""
import pyb 

class Encoder:
    ''' @brief                      interface with quadrature encoders
        @details
    '''
    def __init__(self, pinA, pinB, tim_num):
        ''' @brief                  constructs an encoder object
            @details
        '''
        self.period = 65535

        self.tim = pyb.Timer(tim_num, prescaler = 0, period = self.period)
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_A, pin=pinA)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_B, pin=pinB)
        
        self.count = 0
        self.current_pos = 0
        self.delta = 0
        
    def update(self):
        ''' @brief                  updates encoder position and delta
            @details
        '''
        self.encodercount_1 = self.tim.counter()
        self.delta = self.encodercount_1 - self.count
        if self.delta > self.period/2:
            self.delta -= self.period
        if self.delta < (-1*self.period/2):
            self.delta += self.period
            
        self.count = self.encodercount_1
        self.current_pos += self.delta
    
   
    def get_position(self):
        ''' @brief                  returns encoder position
            @details
            @return                 the position of the encoder shaft
        '''
       
        return self.current_pos
   
    def set_position(self, position):
        ''' @brief                  updates encoder position and delta
            @details
            @param position         the new position of the encoder shaft
        '''
        self.encodercount_1 = position
        print('')
        print('Setting position and delta values')
   
    def get_delta(self):
        ''' @brief                  updates encoder position and delta
            @details
            @return                 the change in position of the encoder 
                                    shaft between the two most recent updates
        '''
        return self.delta