"""@file        main_LAB2.py
   @brief       main code which connects encoder to user task
             
   @author      Aleya Dolorofino
   @author      Tanner Hillman
   @date        October 8, 2021
"""

import Task_User
import Task_Encoder


if __name__ =='__main__':
    '''@brief start of main code
    '''
   
    encoderTask = Task_Encoder.Task_Encoder()
    userTask = Task_User.Task_User()
    NeedCommand = True
         
    while (True):
        #Attempt to run FSM unless Ctrl+c is hit
        try:
            if NeedCommand:
                keyCommand = userTask.run()
                NeedCommand = False
                print ('main key command is')
                print (keyCommand)
                encoder_output = encoderTask.run(keyCommand)
                print('here I am')
            
        #If there is an interuption break
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
