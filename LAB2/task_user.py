"""@file        task_user.py
   @brief       FSM that iteracts with encoders
   @details     Allows the input from user to generate specific actions 
                from the encoder
             
   @author      Aleya Dolorofino
   @author      Tanner Hillman
   @date        October 8, 2021
"""
import pyb
import Encoder
import time
import Task_Encoder

CommReader = pyb.USB_VCP()

S0_INIT = 0
S1_WAIT_FOR_INPUT = 1
S2_ZERO_POSITION = 2
S3_PRINT_POSITION = 3
S4_PRINT_DELTA = 4
S5_COLLECT_DATA = 5
pinA = pyb.Pin(pyb.Pin.cpu.B6)
pinB = pyb.Pin(pyb.Pin.cpu.B7)
tim_num = 4
encoderTask = Task_Encoder.Task_Encoder()

class Task_User:
    ''' @brief   Begins the user task
    '''
    
    def __init__(self):

        ''' @brief              Constructs an encoder object
        '''
        ## The Current state for this iterations of the FSM
        self.state = S0_INIT
        self.encoder = Encoder.Encoder(pinA, pinB, tim_num)

    def transition_to(self, new_state):
        '''@brief method for transitioning between states
           @param new_state input for new state in FSM
        '''
        self.state = new_state;
        
    def run(self):
       '''@brief runs the overal FSM
       '''
       self.state = S0_INIT
       while (True):  
            time.sleep(0.1)
            if(self.state == S0_INIT):
                #print('in state 0')
                self.encoder.update()
                self.state = S1_WAIT_FOR_INPUT
                
            elif(self.state == S1_WAIT_FOR_INPUT):
                if(CommReader.any()):
                    keyCommand = CommReader.read(1)
                    #print ('key command is')
                    #print (keyCommand)
                    
                    if (keyCommand == b'z'):
                        self.state = S2_ZERO_POSITION
                        #print ('state 2')
                        ugh = encoderTask.run(keyCommand)
                    
                    elif (keyCommand == b'p'):
                        self.state = S3_PRINT_POSITION
                        #print ('state 3')
                        ugh = encoderTask.run(keyCommand)
                   
                    elif (keyCommand == b'd'):
                        self.state = S4_PRINT_DELTA
                        #print ('state 4')
                        ugh = encoderTask.run(keyCommand)
                    
                    elif (keyCommand == b'g'):
                        self.state = S5_COLLECT_DATA
                        #print ('state 5')
                        ugh = encoderTask.run(keyCommand)
               
            else:
                break
                    
          
       