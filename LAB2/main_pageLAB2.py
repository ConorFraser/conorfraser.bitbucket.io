'''@file                mainpage_LAB2.py
   @brief               Brief for mainpage_LAB2.py
   @details             Details for mainpage_LAB2.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                         
   @section btn_fcn     Main
                        Runs tasks cooperatively.

   @section sec_mot     Encoder
                        Encoder Driver. Interacts with the quadrature encoders. 
                        <br>
   
    @section st_dgm     User Task
                        Allows the user to interface with the 

    
   @author              Tanner Hillman

   @copyright           License Info

   @date                December 9, 2021
'''