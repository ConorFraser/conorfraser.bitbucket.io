'''@file                mainpage_LAB5.py
   @brief               Brief for mainpage_LAB5.py
   @details             Details for mainpage_LAB5.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>

   @section sec_enm     Main
                        Runs IMU driver on loop. Checks for calibration before returning data.

   @section sec_mot     IMU
                        Interacts with hardware to calibrate, set cal_coeffs, read euler angles, read angular velocites, and runs due to main.

   @section sec_mop     Video
                        IMU calibration:
                        <br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/d4gY1B1i5i4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <br>
                        
   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''