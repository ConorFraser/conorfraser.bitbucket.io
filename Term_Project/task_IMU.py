# -*- coding: utf-8 -*-
''' @file                       task_IMU.py
    @brief                      Task for running the IMU driver.
    @details                    Includes a Task_IMU class. The class contains an init function and nine supporting functions.
    @details                    The run function is continually called from main and is ran cooperatively with all of the other tasks.
    @details                    The other 
    @author                     Jason Davis
    @author                     Conor Fraser
    @author                     Solie Grantham
    @author                     Zachary Stednitz
    @date                       December 1, 2021
'''
import os
import utime, pyb, gc, vector, shares
from micropython import const

S0_init = const(0)
S1_calibrateSensor = const(1)
S2_displayOutput = const(2)
S3_zeroSensor = const(3)

# shares = [motor_share, delta_share, output_share, controller_share, imu_share, panel_share, v]

class Task_IMU:
    
    def __init__(self, taskID, imu, period, shares, kinVector):
        self.imu = imu
        self.taskID = taskID
        self.period = period
        self.imu_share = shares[4]
        self.kinVector = kinVector
        
        self.thetaX = -1
        self.omegaX = -1
        self.thetaY = -1
        self.omegaY = -1
        
        self.errThetaX = float(0)
        self.errThetaY = float(0)
        self.errOmegaX = float(0)
        self.errOmegaY = float(0)
        
        self.calibrationFile = "imu_cal_coeffs.txt"
        
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_init
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        
        gc.collect()
        current_time = utime.ticks_us()
        
        self.updateOrientation()
        
        action = self.imu_share.read()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_init:
                
                if (action == 0):
                    self.transition_to(S1_calibrateSensor)
                    utime.sleep_us(100)
                    self.calibrate()
                # displaying the menu and then advancing to state 1 of the
                # task_user FSM
                    self.imu_share.write(None)
                    self.transition_to(S0_init)
                    
                elif(action == 1):
                    self.transition_to(S2_displayOutput)
                    self.getPanelOrientation()
                    self.imu_share.write(None)
                    self.transition_to(S0_init)
                    
                elif(action == 2):
                    self.transition_to(S3_zeroSensor)
                    self.zeroPanelOrientation(False)
                    self.imu_share.write(None)
                    self.transition_to(S0_init)
                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            # self.runs += 1
    
    def calibrate(self):
        data = self.imu.get_calibration_data()
        print("Calibration Value: " + str(data))
        #print('test calibration loop runs')
        utime.sleep(.25)
        
        if(data[0] + data[1] + data[2] + data[3] == 12):     # checking to see if calibration is complete by confirmng all four 
            self.imu.setCalibrationStatus(True)
            # print('Calibration completed \n'
            #       'Calibration Status: '+str(self.imu.get_calibration_coefficient()))
    
    def updateOrientation(self):
        a = self.imu.read_euler_angles()
        b = self.imu.read_angular_velocity()
        
        self.thetaX = a[0]
        self.thetaY = a[1] 
        self.omegaX = b[0] 
        self.omegaY = b[1] 
        
        self.kinVector.setThetaX(self.thetaX)
        self.kinVector.setThetaY(self.thetaY)
        self.kinVector.setOmegaX(self.omegaX)
        self.kinVector.setOmegaY(self.omegaY)
    
    def getIMU(self):
        return self.imu
    
    def getPanelOrientation(self):
        print('(thetaX, thetaY):  ({0}, {1})'.format(self.kinVector.getThetaX(), self.kinVector.getThetaY()))
        print('(omegaX, omegaY):  ({0}, {1})\n'.format(self.kinVector.getOmegaX(), self.kinVector.getOmegaY()))
        
    def zeroPanelOrientation(self, auto):
       
        print('zeroing panel orientation')
        temp = []
        
        if (self.calibrationFile in os.listdir() and auto == True):
            # File exists, read from it
            print(str(self.calibrationFile) + ' found! Reading contents...',end="")            
            f = open(self.calibrationFile, "r")
            
            for line in f:
                temp.append(line)
                
            f.close()
            
            self.errThetaX = temp[0]
            self.errThetaY = temp[1]
            self.errOmegaX = temp[2]
            self.errOmegaY = temp[3]            
            print('done')
            
        else:
        # File doesnt exist, calibrate manually and 
        # write the coefficients to the file
            # print('{0} does not exist.'.format(self.calibrationFile))
            print('Move the panel to a flat and level orientation.')
            input('Press [enter] when ready to set home position')
            self.updateOrientation()
            print('Establishing offsets...',end='')
            t1 = self.thetaX
            t2 = self.thetaY
            t3 = self.omegaX
            t4 = self.omegaY
            
            self.kinVector.setErrThetaX(t1) 
            self.kinVector.setErrThetaY(t2)
            self.kinVector.setErrOmegaX(t3)
            self.kinVector.setErrOmegaY(t4)
            print('done')
        
            print('Writing contents to ' + str(self.calibrationFile) + '...',end='')
            f = open(self.calibrationFile, "w+")
            f.write("{0}\n".format(t1))
            f.write("{0}\n".format(t2))
            f.write("{0}\n".format(t3))
            f.write("{0}\n".format(t4))
            f.close()
            print('done\n')
        
    def setHomeOffsets(self):
        # print('Retrieving home offsets...', end='')

        if self.calibrationFile in os.listdir():
            # File exists, read from it
            f = open(self.calibrationFile, "r")
            print('Reading data from ...{0}'.format(self.calibrationFile),end='')
            
            t1 = f.read()
            t2 = f.read()
            t3 = f.read()
            t4 = f.read()
            f.close()
            
            print('applying home offsets...',end='')
            self.kinVector.setErrThetaX(t1) 
            self.kinVector.setErrThetaY(float(t2))
            self.kinVector.setErrOmegaX(float(t3))
            self.kinVector.setErrOmegaY(float(t4))
            print('done')

    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        # if (self.dbg):
        #     print('{:}: S{:}->S{:}'.format(self.taskID ,self.state,new_state))
        self.state = new_state
        