var classclosed__loop__controller_1_1closedLoopController =
[
    [ "__init__", "classclosed__loop__controller_1_1closedLoopController.html#a0dc5fa849e275cc33afff0c87c9fbd06", null ],
    [ "active", "classclosed__loop__controller_1_1closedLoopController.html#a4f2e8442f47a34e13af09aaf23695124", null ],
    [ "computeTorques", "classclosed__loop__controller_1_1closedLoopController.html#a6201e00efec6689fde0163e2f1832e40", null ],
    [ "displayGainMatrix", "classclosed__loop__controller_1_1closedLoopController.html#ad12a1c58768d47eb776b957fcba3a195", null ],
    [ "getDutyVector", "classclosed__loop__controller_1_1closedLoopController.html#aac717f114ca6821429c398fbf88189fa", null ],
    [ "getGainMatrices", "classclosed__loop__controller_1_1closedLoopController.html#aca145ca2e1a8ea4f62a69b60442b6153", null ],
    [ "getTx", "classclosed__loop__controller_1_1closedLoopController.html#aa744382de7a94ce871ab95abc07034a5", null ],
    [ "getTy", "classclosed__loop__controller_1_1closedLoopController.html#a570dd45bc52399f18cf7e82eefe718ae", null ],
    [ "initializeGainMatrices", "classclosed__loop__controller_1_1closedLoopController.html#a9ab83dc5e780380ca53067f54896b417", null ],
    [ "modifyGainMatrix", "classclosed__loop__controller_1_1closedLoopController.html#a7769048c6cac1c8216c86be3483d065c", null ],
    [ "saveGainMatrices", "classclosed__loop__controller_1_1closedLoopController.html#ab9423289599fa44125c306f41df25472", null ],
    [ "toggleActive", "classclosed__loop__controller_1_1closedLoopController.html#adf5ae2dd128807edef9eb6d2ed208c55", null ]
];