/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Term Project", "index.html", [
    [ "To Return the to Main Portfolio...", "index.html#rtr_bck", null ],
    [ "ME 305 Term Project Cover Page", "index.html#tst_rpz", null ],
    [ "Driver List:", "index.html#tst_rpx", null ],
    [ "IMU driver", "index.html#tst_rpc", null ],
    [ "Panel Driver:", "index.html#tst_rvv", null ],
    [ "Motor Driver:", "index.html#tst_rbb", null ],
    [ "Motor 1:", "index.html#tst_oot", null ],
    [ "Motor 2:", "index.html#tst_rll", null ],
    [ "Closed Loop Controller Driver:", "index.html#tst_rkk", null ],
    [ "Our task list contains:", "index.html#tst_rhh", null ],
    [ "Methods of transferring data:", "index.html#tst_rrr", null ],
    [ "Helpful Code We Added:", "index.html#tst_eee", null ],
    [ "Issues with the final product:", "index.html#tst_qqq", null ],
    [ "Final Video", "index.html#fnl_www", null ],
    [ "FSMs and Task Diagrams", "index.html#fsm_ttk", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';