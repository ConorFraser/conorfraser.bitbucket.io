var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#adf619d380e6b489cbabbdb2043855449", null ],
    [ "change_operation_mode", "classBNO055_1_1BNO055.html#a39de47ae042cd78050d3374d06b92782", null ],
    [ "get_calibration_coefficient", "classBNO055_1_1BNO055.html#a37d99d3467b67b4600b4b70b31ab1dec", null ],
    [ "get_calibration_data", "classBNO055_1_1BNO055.html#a9885a9af876bfd6aaace6f012d7c30aa", null ],
    [ "getCalibrationStatus", "classBNO055_1_1BNO055.html#aef2667f4ebaeb9a863f96435bbd6181f", null ],
    [ "read_angular_velocity", "classBNO055_1_1BNO055.html#a219fa1f4c5771a91449f75b1b7bb0911", null ],
    [ "read_euler_angles", "classBNO055_1_1BNO055.html#a9dad2b1fb181dec25f5b15f0bf509e1e", null ],
    [ "setCalibrationStatus", "classBNO055_1_1BNO055.html#a835df25edaea373d143a9db150b8289d", null ],
    [ "write_calibration_coefficient", "classBNO055_1_1BNO055.html#a2f601c9425b0e383285c3a9ff5495179", null ]
];