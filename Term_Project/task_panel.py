"""
@file       task_panel.py
@brief      Responsible for running the tasks related to reading from our touch panel.
@details    When the tasks are run, we are able to get the position of the 
            ball in x and y directions and use an alpha beta filter to 
            determine the velocity. We then share this information in a kinematic
            vector matrix that can be shared between files. 

@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
"""
import os
import panelDriver
from ulab import numpy
import utime, pyb, gc, vector, shares
from micropython import const

S0_init = const(0)
S1_calibratePanel = const(1)
S2_displayCoordinates = const(2)

class Task_Panel:
    
    def __init__(self, taskID, devices, shares, panelDriverObject, dbg = False):
        ''' @brief          Constructor for Task_Panel class
            @param          panelDriverObject A panel object we can use to calculate ball position
        '''
        self.taskID = taskID
        
        self.panelDriverObject = devices[5]
        
        self.panel_share = shares[6]
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        self.dbg = dbg
        
    def run(self, kinVector):
        ''' @brief      Runs the tasks related to the panel.
            @details    This function gets the postion of the ball and velocity in both
                        the x and y directions. This is then placed in the kinemeatic 
                        vector to be shared among the rest of the program. 
            @param      kinVector Vector that stores the position and velocity of the ball. 
        '''
        self.kinVector = kinVector
        action = self.motor_share.read()
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
        
        #while (True):
            if (self.panelDriverObject.panelTouch()):
                # Read x y z coordinates
                xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
                # Read the alpha beta filter values.
                x_filter, x_dot_filter, y_filter, y_dot_filter, z = self.panelDriverObject.filtering(utime.ticks_us(50))
                x1 = xCoordinate
                x_dot1 = x_dot_filter
                y1 = yCoordinate
                y_dot1 = y_dot_filter
                # print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                # Assign the values read from above to the kinematic vector to be
                # used in the main file. 
                self.kinVector.setX(x1)
                self.kinVector.setY(y1)
                self.kinVector.setXDot(x_dot1)
                self.kinVector.setYDot(y_dot1)
            else:
                print("Touch the panel")
    def collectBufferedInput(self, motorID):
        print('Enter a duty cycle for {0}: '.format(motorID), end = '')
        
        userInput = str(self.ser.read(2))
        if (len(userInput) > 4):
            t = userInput[2:4]
        else:
            t = userInput[2:3]
        temp = list([])
        
        # hitting the 'enter' key sends \r character to the VCP
        while (t != '\\r'):
            
            # append the information in the VCP to "duty" only if it is a digit
            if (t.isdigit() or t == '-'):
                # print('t is a digit')
                print ('{0}'.format(t), end = '')
                temp.append(t)
            
            userInput = str(self.ser.read(2))
            if (len(userInput) > 4):
                t = userInput[2:4]
            else:
                t = userInput[2:3]
            
            # NOTE: everytime we read values from the VCP, it then EMPTIES the VCP
            # We need to store the value of the VCP in a temp variable if we wish to use it later
        duty = ''.join(map(str, temp))
        # print('duty is: |{0}|'.format(duty))
        if (duty == ''):
            duty = self.motor.getDuty() * self.motor.getDirection()
        # handling the case of duty > 100
        elif (int(duty) > 100):
            duty = 100
            
        # handling the case of duty < 0
        elif (int(duty) < -100):
            duty = -100
        return duty
    
    def calibrate(self):
        ''' @brief      Checks if a calibration file is present, and manually calibrates if not. 
            @details    When manually calibrating the panel, there are pre defined calibration
                        points that can be used to calibrate the panel. The user is prompted to 
                        tap them and then we write the calibration coefficients to the 
                        nucleo in a text file so we don't need to calibrate every time. 
        '''
        
        filename = "RT_cal_coeffs.txt"

        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
        else:
            # tapPoints = numpy.array([[-80, -40], [80, -40], [-80, 40], [80, 40], [0, 0]])
            # uncalPoints = numpy.zeros(5,2)
            # for i in range(5):
            #     print("Begin manual calibration...")
            #     print("Press point ({}, {})".format(tapPoints[i,0], tapPoints[i,1]))
            #     while (True):
            #         try:
            #             if (self.panelDriverObject.panelTouch()):
            #                 xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
            #                 uncalPoints[i,0] = xCoordinate
            #                 uncalPoints[i,1] = yCoordinate
            #                 uncalPoints[i,2] = 1
            #                 break
            #         except:
            #             KeyboardInterrupt()
            sensorADC = []
      
        
            prompts = ['Upper Left',        # -80, 40
                       'Upper Right',       #  80, 40
                       'Lower Right',
                       'Lower Left',
                       'Center']
            XYcoords = [[-80, 40],
                      [ 80, 40],
                      [ 80,-40],
                      [-80,-40],
                      [0,0]]
            
            for coord,prompt in zip(XYcoords,prompts):
                print(f"Please press {prompts} at {XYcoords}")
                while not panelDriverObject.panelTouch():
                    pass
                sensorADC.append([self.panelDriverObject.scanX(), self.panelDriverObject.scanY(), 1])
                
                                
                # XYsensor.append(coord)
            print(sensorADC)
            print(XYcoords)
            

            return sensorADC
                        
            with open(filename, 'w+') as f:
                # calibrate manually, write coeff. to file
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = panelDriver.calibSensorOnPanel(sensorADC)
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                
if __name__ == '__main__' :
    
     Xp = pyb.Pin.board.PA7
     Xm = pyb.Pin.board.PA1
     Yp = pyb.Pin.board.PA6
     Ym = pyb.Pin.board.PA0
     xDim = 176
     yDim = 100
    
     inputPins = [Xp, Xm, Yp, Ym]    
     panelDims = [xDim, yDim]
    
     panelDriverObject = panelDriver.panelDriver(inputPins, panelDims)
     Task_PanelObject = Task_Panel(panelDriverObject)
     Task_PanelObject.calibrate()
    
     while (True):
        try:
            if (panelDriverObject.panelTouch()):
                xCoordinate = panelDriverObject.scanX()
                yCoordinate = panelDriverObject.scanY()
                sensorADC = panelDriverObject.calibSensorOnPanel()
                print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                utime.sleep_ms(1000)
            else:
                pass
        
        except KeyboardInterrupt:
            break
     print('\n*** Program ending, have a nice day! ***\n')