'''@file                mainpage_Lab1.py
   @brief               Brief for mainpage_Lab1.py
   @details             Details for mainpage_Lab1.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                         
   @section btn_fcn     Button Press Function
                        The button switches the state of the FSM

   @section sec_mot     Main Function
                        Runs the program
                        <br>
   @section st_dgm      State Transition Diagram
                        <br>
                        \image html Lab01FSM.PNG
                        <br>
                        
   @section opr_vid     Operation 
                        This video demostrates the operation of the system:
                        <br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/C5RmhoZzLCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        <br>
    
   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''