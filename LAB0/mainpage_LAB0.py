'''@file                mainpage_LAB0.py
   @brief               Brief for mainpage_Lab0.py
   @details             Details for mainpage_Lab0.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                         
   @section fib_fcn     Fibonacci Function
                        Find fionacci number at given index.

   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''